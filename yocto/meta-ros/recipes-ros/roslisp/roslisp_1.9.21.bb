DESCRIPTION = "Lisp client library for ROS"
SECTION = "devel"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://package.xml;beginline=7;endline=7;md5=d566ef916e9dedc494f5f793a6690ba5"

DEPENDS = "cmake-modules"

SRC_URI = "https://github.com/ros/${ROS_SPN}/archive/${PV}.tar.gz;downloadfilename=${ROS_SP}.tar.gz"
SRC_URI[md5sum] = "f551dcf3b8cde82413aaef1db609a8cf"
SRC_URI[sha256sum] = "cc47259f9d35856cdc0470e0181f9fcfc02bbd00c894eef624bc451320131794"

S = "${WORKDIR}/${ROS_SP}"

inherit catkin
